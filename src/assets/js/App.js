var App = function () {

    this.init = function () {
      this.menuHandler();
      this.formHandler();
    };

    this.menuHandler = function () {
      var self = this,
        $menuItems = $('#main-navbar a');

      $menuItems.on('click', function (e) {
        var target = $(this).attr('href');
        if ($(target).length) self.scrollToEl(target, 0);
        e.preventDefault();
      })
    };

    this.formHandler = function () {
      var $form = $('form');

      $form.on('submit', function (e) {
        e.preventDefault();

        var data = {
            name: $('#input-name').val(),
            email: $('#input-email').val(),
            tel: $('#input-tel').val()
        };

        /*data = $('form input').map(function (i, el) {
          var obj = {};
          obj[el.name] = el.value;
          return obj;
        });*/
        console.log(data);


      })
    };

    // additional
    this.scrollToEl = function (el, offset, duration) {
        var $el = !( el instanceof jQuery ) ? $(el) : el;
        el = $el.length ? $el : $('body');
        offset = typeof offset !== 'undefined' ? offset : -120;
        duration = duration || 1000;
        $('html, body').stop().animate({
            scrollTop: el.offset().top+offset
        }, duration);
    };

};
